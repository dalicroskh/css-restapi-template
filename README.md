CSS REST API template example
==============================

Example REST API. When started, listens on port 3000

ENV
-----
* SERVICE_NAME - Name of the service, default ""
* START_TIMEOUT - Timeout before service changes state from starting to ready [ms], default 60000
* JWT_SECRET - Secret phrase used for JSON token generation, default "CSSjeSoucastCSG"
* LOGIN_KEY - Login key used for REST API user authorization, default "123456789abcdefg"

API
-----

GET /
Default end-point, requires authorization token in the header

RESPONSE BODY EXAMPLE
>{
"serviceName": "CSS REST API template example",
"timestamp": "2022-03-04T16:21:08.045Z"
} 

POST /auth/login
Login endpoint

REQUEST BODY EXAMPLE
>{
"user":"CSS_user",
"loginKey":"123456789abcdefg"
} 

RESPONSE BODY EXAMPLE
>{
"user": "CSS_user",
"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiQ1NTX3VzZXIiLCJpYXQiOjE2NDY0MTA5ODl9.LsqyPqPYCmTvfKkznCoCsr5w6Wk_W-qFsnhYe0Dj3YM"
}

POST /auth/logout
Logout endpoint, requires authorization token in the header

GET /health
Health end-point, requires authorization token in the header

RESPONSE BODY EXAMPLE
>{
"serviceName": "CSS REST API template example",
"status": "running",
"timestamp": "2022-03-04T16:25:44.577Z",
"user": "CSS_user"
}

GET /health/live
Health live end-point, requires authorization token in the header

RESPONSE BODY EXAMPLE
>{
"user": "CSS_user"
}

GET /health/live
Health running end-point, requires authorization token in the header

RESPONSE BODY EXAMPLE
>{
"user": "CSS_user"
}

Usage
-----

>docker-compose up -d

