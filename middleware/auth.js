
const jwt = require('jsonwebtoken');
const secrets = require('../secrets/secrets');
let validTokens = [];

const auth = async (req, res, next) => {

    try {

        const token = req.header('Authorization').replace('Bearer ','');

        const validatedTokens = validTokens.filter((item) => item === token);

        if(validatedTokens.length === 0) throw new Error();

        const decoded = jwt.verify(token, secrets.jwtsecret);

        req.user = decoded.user;
        req.token = token;
        next();
    }
    catch (err) {
        res.status(401).send({error:"Please authorise."})
    }
}

const addToken = (token) => validTokens.push(token);
const removeToken = (token) => validTokens = validTokens.filter((item) => token !== item)

module.exports = {
    auth,
    addToken,
    removeToken,
};