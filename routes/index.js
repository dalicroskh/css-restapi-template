const express = require('express');
const {auth} = require("../middleware/auth");
const router = express.Router();

const serviceName = process.env.SERVICE_NAME || 'CSS REST API template example';

/* GET */
router.get('/', auth, function(req, res) {
  const timestamp = new Date(Date.now());
  res.status(200).send({serviceName, timestamp, user:req.user});
});

module.exports = router;
