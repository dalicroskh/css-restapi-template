const express = require('express');
const router = express.Router();

const {auth} = require('../middleware/auth');

const serviceName = process.env.SERVICE_NAME || 'CSS REST API template example';
let status = 'starting';
const startTimeout = Number.parseInt(process.env.START_TIMEOUT) || 60000

setTimeout(() => status = 'running', startTimeout);

/* GET users listing. */
router.get('/', function(req, res, next) {
    const timestamp = new Date(Date.now());
    res.send({serviceName, status, timestamp, user:req.user});
});

router.get('/live',  (req, res) => res.status(200).send({user:req.user}));

router.get('/ready', (req, res) => {

    if(status === 'starting') {
        res.status(503).send({user:req.user});
    }
    else if(status === 'running') {
        res.status(200).send({user:req.user});
    }
    else {
        res.status(500).send();
    }
});

module.exports = router;