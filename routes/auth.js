const express = require('express');
const jwt = require('jsonwebtoken');

const secrets = require('../secrets/secrets');
const {removeToken, addToken, auth} = require("../middleware/auth");
const router = express.Router();

router.post('/login', async (req, res) => {
    try {

        if(!req.body || !req.body.loginKey || !req.body.user || secrets.loginKey !== req.body.loginKey) throw new Error('Login failed');

        const token = await jwt.sign({user:req.body.user}, secrets.jwtsecret);

        addToken(token);

        const user = req.body.user;

        res.send({user, token});
    }
    catch (err) {
        res.status(400).send({error:err.message})
    }
});

router.post('/logout', auth, (req, res) => {
    removeToken(req.token);

    res.send();
});

module.exports = router;